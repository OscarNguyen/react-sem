import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';
import Attendance from './components/Attendance';

function App() {
  return (
    <div className="App">
      <Attendance title="Attendances code for Timo's course" />
    </div>
  );
}

export default App;
