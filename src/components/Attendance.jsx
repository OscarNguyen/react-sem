import React, { useState, useEffect } from 'react';
import ReactTable from 'react-table-v6';
import 'react-table-v6/react-table.css';
import axios from 'axios';
const Attendance = ({ title }) => {
  const [attendance, setAttendace] = useState([]);

  const columns = [
    {
      Header: 'Id',
      accessor: 'id',
    },
    {
      Header: 'Key',
      accessor: 'key',
    },
  ];

  useEffect(() => {
    fetch('/attendances', {
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        Accept: 'application/json; odata=verbose',
        Authorization: 'Basic ' + btoa('user:123456'),
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setAttendace(data);
      });

    // const result = axios.get('https://e1800917sdemo.herokuapp.com/attendances');
    // console.log(result.data);
  }, []);

  return (
    <div>
      <h4>{title}</h4>
      <ReactTable data={attendance} columns={columns} />
    </div>
  );
};

export default Attendance;
